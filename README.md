# 需求分析



## 引言

&nbsp;

### 背景

TDengine作为一款专为物联网、车联网、工业互联网、运维监测等优化而设计的时序大数据平台，具有高效的存储、查询、分析的性能。目前已经提供命令行界面风格的客户端工具 taos shell（具体部署参见官网文档www.taosdata.com/cn/documentation/getting-started），本项目期望将这一工具移植为 Web 应用在浏览器中运行，既兼具web及时响应的特点，又能方便地获取TDengine的服务。

### 概述

1)  前端界面作为直接与用户交互的界面，至少包含以下特点：

&nbsp;

i.  至少包含供用户输入指令的组件，以及返回指令执行结果的组件，界面尽可能简洁友好，易于操作。

ii. 对返回结果是二维表的，提供排序，筛选功能，同时支持对历史指令和历史结果的回溯。

iii. 对所有出错的信息能及时以显眼的方式反馈到前台，并能大致分析出错的原因，提供解决的大致思路。

iv. 在前端完成sql语法检测，直到输入格式正确的sql语句才发生到服务器

v.  前端提供一定的帮助文档帮助用户了解产品及其使用方法，迅速上手使用。

&nbsp;

2)  服务器需求

&nbsp;

i.  解析前台发来的sql语句，执行相应的命令。

ii. 完成高性能的处理，查询，聚合功能。

iii. 能处理高并发访问，提供高可用的性能。

iv. 服务器和网络出现任何问题能及时生成错误代码，反馈到前台。

&nbsp;

## 总体描述

&nbsp;

### 产品描述

通过将原有的taos shell命令行风格工具移植到web上，免去用户客户端安装的麻烦，在任何具备网络连接和浏览器工具的设备上方便地进行数据操作和系统的管理维护。Taos SQL在一定程度上提供类似于标准 SQL 类似的风格和模式, 本产品提供TAOS SQL作为数据写入和查询的主要工具，便于用户快速上手，对系统进行管理维护。

### 用户要求

具有一定sql基础更易上手和理解本产品。

不具备sql基础也能提供阅读帮助文档迅速上升。

### 产品功能

&nbsp;

1)  支持的sql语句

shell语句以；为结束标志，一条语句可分多行输入，直到遇到；结束读入进行解析，且对大小写不敏感。

a.  对数据库数据表的增删查改

数据库对象的管理主要是增删改查，主要通过create，drop，alter，show，describe五个动词实现，以下将分对象展示语句具体实现形式

Database：

i.  建立数据库：create database \[if not exists\] db_name \[KEEP days\] \[update 1\];

ii. 删除数据库：drop database \[if exists\] db_name (执行后该数据库的所有表被删除，谨慎使用)

iii. 修改数据库参数：alter database db_name \<参数名> \<参数值>

iv. 切换当前使用数据库：use da_name (由于本产品使用restful接口发起连接，是无状态的，该指令无法起作用，故所有对表名、超级表名的引用都需要指定数据库名前缀，此项功能在后续版本可能会支持)

v.  显示数据库信息：show databases

vi. 显示一个数据库的创建语句：show create database db_name

Stable：

i.  创建超级表：CREATE STABLE \[IF NOT EXISTS\] stb_name (timestamp_field_name TIMESTAMP, field1_name data_type1 \[, field2_name data_type2 \...\]) TAGS (tag1_name tag_type1, tag2_name tag_type2 \[, tag3_name tag_type3\]);

ii. 删除超级表：drop stable \[if exists\] stb_name (执行后会自动删除通过stable建立的子表)

iii. 修改超级表的结构：alter stable stb_name \<add\|drop\|modify> column filed_name \[data_type\]

iv. 获取超级表的结构：describe stb_name (需先指定database)

v.  Alter stable stb_name \<add\|drop\|change\|modify> tag tag_name \<date_type\|new_tag_name>

Table:

i.  创建数据表（以超级表为模板）：CREATE TABLE \[IF NOT EXISTS\] tb_name USING stb_name TAGS (tag_value1, \...);

ii. 不以超级表为模板：CREATE TABLE \[IF NOT EXISTS\] tb_name (timestamp_field_name TIMESTAMP, field1_name data_type1 \[, field2_name data_type2 \...\]);

iii. 删除数据表：drop table \[if exists\] tb_name

iv. 显示当前数据库下的数据表信息：show tables \[like tb_name_wildcar\] (like 后加通配符进行匹配查询)

v.  显示建表语句：show create table tb_name

    b.  数据写入

&nbsp;

i.  可以同时在多个表中插入多条记录（效率更高）：

insert into table_name1 values (...) \[ (...) \]

table_name2 values (...)

ii. 在插入时自动建表：

Insert into table_name using stable_name tags (...) values (...)

iii. 插入来自CSV文件的记录（无需表头）：

insert into table_name FILE '(文件地址)'

iv. 针对 insert 类型的 SQL 语句，我们采用的流式解析策略，在发现后面的错误之前，前面正确的部分 SQL 仍会执行。（如类型ii,即使values不匹配，原先不存在的表也会被建立）

    c.  数据查询

查询语法：

SELECT select_expr \[, select_expr \...\]

FROM {tb_name_list}

\[WHERE where_condition\]

\[SESSION(ts_col, tol_val)\]

\[STATE_WINDOW(col)\]

\[INTERVAL(interval_val \[, interval_offset\]) \[SLIDING sliding_val\]\]

\[FILL(fill_mod_and_val)\]

\[GROUP BY col_list\]

\[ORDER BY col_list { DESC \| ASC }\]

\[SLIMIT limit_val \[SOFFSET offset_val\]\]

\[LIMIT limit_val \[OFFSET offset_val\]\]

\[\>\> export_file\];

> 查询语法根据实现场景的不同，有多种不同的实现方式，此处仅对其中几个重要参数做解释，其他可查阅官方文档了解（<https://www.taosdata.com/cn/documentation/taos-sql#select>）

i.  通配符 \* 在普通表中不包含标签列（可以通过输入标签列名查询获得），在超级表中则包含标签列。

ii. FROM关键字后面可以是若干个表（超级表）列表，也可以是子查询的结果。 如果没有指定用户的当前数据库，可以在表名称之前使用数据库的名称来指定表所属的数据库。

iii. 检查客户端版本：select client_version()

iv. 服务器状态检测语句。如果服务器正常，返回一个数字（例如 1）。如果服务器异常，返回error code。该SQL语法能兼容连接池对于TDengine状态的检查及第三方工具对于数据库服务器状态的检查。并可以避免出现使用了错误的心跳检测SQL语句导致的连接池连接丢失的问题。

select server_status()

其他如where支持的条件过滤操作，嵌套查询，sql函数等与标准sql类似，在官网文档可查询到


# 架构设计
## RESTful架构
REST 是 Representational State Transfer的缩写，如果一个架构符合REST原则，就称它为RESTful架构

RESTful 架构可以充分的利用 HTTP 协议的各种功能，是 HTTP 协议的最佳实践

返回的是JSON类型数据，也便于前端处理
## MVVM数据驱动
MVVM架构和MVC架构一样，主要目的是分离视图（View）和模型（Model），有以下几大优点：

1低耦合：View可以独立于Model变化和修改，通过ViewModel做双向绑定，这也是最核心的要点
2.独立开发：因为低耦合性，前端界面可以独立于后台开发，独立进行测试，而不用与后台交互
其他优点不一一赘述

由于MVVM架构开发时具有数据驱动的特点，此处简述本项目的**数据逻辑**：
1.界面分两部分（两个template实现）：**输入服务器地址的login页面** 和**执行sql语句的webshell页面 **

其中login页面记录登录信息（loginform），作为以后每次发起rest请求的依据，该数据可通过logout按钮修改

2.webshell页面主要由**三项数组数据**驱动视图更新：tableData，curData,shownData
tableData在每次执行sql语句及查询历史指令结果时获得更新

curData在筛选时间范围时更新

由于物联网单个设备在短时间内可以产生大量数据，由此执行select查询时前台可能返回大量数据（最多10240条记录），带给前台的渲染压力很大，必须分页展示数据，shownData就是在修改页码时获得更新

# 安装使用说明
## 安装
本项目旨在使用户通过web直接访问服务器，进行TDengine数据库的管理，免于安装客户端的繁琐，故**无需安装**

## 使用
### 登录界面：每一项都带有默认值，各项说明如下：
1.host：连接到服务器的域名（需部署由TDengine且能正常启动），可根据实际修改，否则**默认为localhost（本地地址）**
2.port：连接到服务的端口，一般在安装时默认为6030（在taos.conf文件中可修改）
3.username：由于rest连接是无状态的，故必须在每次连接附带身份信息，并且通过Base64编码发送到服务器，在安装时默认为root（此项一般不修改）
4.password： 如3解释，默认为taosdata（此项一般也不修改）

![登录界面图片](./usage_image/1.png)

### 登录时验证规则
为避免无效域名和端口发起连接，浪费网络带宽，故如果无法满足验证规则，无法发起连接
![验证规则](./usage_image/2.png)

### webshell界面
用户选定域名端口登录成功后，前端通过浏览器提供的Storage API保存登录信息，下次登录时**自动连接**
同时界面提供logout选项，**随时可以登出**，为保证安全，登出后保存的登录信息包括历史指令和结果同时删除
![webshell界面](./usage_image/3.png)

### 指令执行
界面提供input输入框供输入执行sql语句，结果实时返回渲染到表格组件中
为避免大量数据同一页面中渲染，表格分页展示数据，每页展示10条数据
![数据展示](./usage_image/4.png)

### 排序和筛选
如果带查询得到的数据带时间戳类型，前端还提供排序和筛选功能（暂时仅提供对时间戳类型数据的排序和筛选，因为在物联网的应用场景中，数据有很强的时序性特征，大部分时候关心的是某个时间段产生的数据）

如图数据根据时间戳列降序排列，可点击或键入选择事件段
![排序和筛选](./usage_image/5.png)
### 历史指令和查询结果回溯
在input输入框内提供方向上下键可回溯历史指令
将鼠标移到”查询历史结果“组件上时，可以看到进十条输入的指令，选择其中一条后会展示其执行结果，打开调试器可以看到并未发送网络请求，说明历史结果的回溯是通过将执行结果存在不本地实现的
![回溯](./usage_image/6.png)

